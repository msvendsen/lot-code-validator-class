<?php

class LotCodeValidator {
	
	private $codes;

	/*
	to be used when instantiating with an array of lot codes
	
	useage:
	$validator = new LotCodeValidator($lotCodes);
	*/
	public function __construct($lotCodes){
		$this->codes = array_map('strtolower', $lotCodes);
		sort($this->codes);
	}

	/*
	to be used when importing a file full of lot codes
	
	useage:
	$validator = LotCodeValidator::withFile('fileName', ',');
	*/
	public static function initWithFile($lotCodeFileLocation, $delimiter){
		if(file_exists($lotCodeFileLocation)){
			$instance = new self( explode($delimiter, file_get_contents($lotCodeFileLocation)) );
			return $instance;
		} else {
			throw new Exception('LotCodeValidator::initWithFile -- The file can not be found');
		}
	}

	public function validateCode($inputCode){
		$inputCode = strtolower($inputCode);
		return in_array($inputCode, $this->codes);
	}

}