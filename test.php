<?php

require_once('LotCodeValidator.php');

/*
Testing from array input
*/

/*
print 'start test';

$lotCodes = array(
	'abcdefg',
	'hijklmn',
	'opqrstu',
	'vwxyz123'
	);

$validator = new LotCodeValidator($lotCodes);
var_dump($validator->validateCode('abcdefg'));
var_dump($validator->validateCode('asdfasfa'));
*/

/*
Testing from file input
*/

print 'start test';

$validator = LotCodeValidator::initWithFile('3mil-codes.txt', ',');
var_dump($validator->validateCode('abcdefg'));
var_dump($validator->validateCode('K7TFHJJJX'));